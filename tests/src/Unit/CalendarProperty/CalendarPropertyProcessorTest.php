<?php

namespace Drupal\Tests\ics_field\Unit\CalendarProperty;

use Drupal\ics_field\CalendarProperty\CalendarPropertyProcessor;
use Drupal\ics_field\Exception\CalendarDownloadInvalidPropertiesException;
use Drupal\Tests\UnitTestCase;

/**
 * A class for testing the CalendarPropertyProcessor.
 *
 * @group ics_field
 */
class CalendarPropertyProcessorTest extends UnitTestCase {

  /**
   * @var \Drupal\ics_field\CalendarProperty\CalendarPropertyProcessor
   */
  protected $cpp;

  /**
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $ce;

  /**
   * @inheritDoc
   */
  protected function setUp(): void {

    // Mock the content entity.
    $this->ce = $this->getContentEntityMock();

    $this->cpp = new CalendarPropertyProcessor($this->getTokenMock(),
                                               $this->getTzpMock(),
                                               'date_field_reference',
                                               'date_field_uuid'
    );

    parent::setUp();

  }

  /**
   * Tests calendar properties validation.
   *
   * We just test that it throws an error when it needs to,
   * and not that the drupal translation string service does its job properly.
   * Because we mock the service we just return the unprocessed input.
   *
   * @dataProvider propertyProvider
   */
  public function testCheckPropertiesMissingProperty($property) {

    $props = array_combine($this->cpp->getEssentialProperties(),
                           $this->cpp->getEssentialProperties());
    unset($props[$property]);

    // Exception expectations.
    $this->expectException(CalendarDownloadInvalidPropertiesException::class);
    
    $this->cpp->getCalendarProperties([$property], $this->ce, 'http');
  }

  /**
   * Test passing validation with a missing date list.
   */
  public function testWithEmptyDateList() {

    $props = array_combine($this->cpp->getEssentialProperties(),
                           $this->cpp->getEssentialProperties());

    $expected = [
      'timezone'           => 'timezone',
      'product_identifier' => 'product_identifier',
      'uuid'               => 'uuid',
      'summary'            => 'summary',
      'dates_list'         => [],
    ];

    $this->assertEquals($expected,
                        $this->cpp->getCalendarProperties($props,
                                                          $this->getContentEntityMock()));

  }

  /**
   * Test setting the time to midnight for a date without time.
   */
  public function testWithTimelessDateList() {

    $expectedDate = '2020-11-17T00:00:00';

    $contentEntity = $this->getContentEntityWithDateTimeMock('2020-11-17');

    $calendarProperties = $this->cpp->getCalendarProperties(['summary' => 'summary'], $contentEntity);

    $this->assertArrayHasKey('dates_list', $calendarProperties, 'Dates_list array property is set');
    $this->assertEquals($expectedDate, $calendarProperties['dates_list'][0],
      'Added midnight as time to input date missing time information');
  }

  /**
   * Test passing validation with all properties.
   */
  public function testWithAllValidProperties() {

    $props = array_combine($this->cpp->getEssentialProperties(),
                           $this->cpp->getEssentialProperties());

    $expected = [
      'timezone'           => 'timezone',
      'product_identifier' => 'product_identifier',
      'uuid'               => 'uuid',
      'summary'            => 'summary',
      'dates_list'         =>
        [
          0 => '2020-11-17T00:00:00',
        ],
    ];

    $this->assertEquals($expected,
                        $this->cpp->getCalendarProperties($props,
                                                          $this->getContentEntityWithDateTimeMock()));

  }

  /**
   * Test setting and getting essential properties.
   */
  public function testGetSetEssentialProperties() {

    $data = ['property A', 'property B'];
    $this->cpp->setEssentialProperties($data);
    $this->assertEquals($data, $this->cpp->getEssentialProperties());

  }

  /**
   * Provides an array of calendar properties arrays.
   *
   * Data Provider building is done before set up, so we must also construct a
   * CPP here to get the essential properties.
   *
   * @return array
   */
  public function propertyProvider() {

    $token = $this->getTokenMock();
    $tzp = $this->getTzpMock();
    $tr = $this->getTranslationManagerMock();
    $cpp = new CalendarPropertyProcessor($token,
                                         $tzp,
                                         'date_field_reference',
                                         'date_field_uuid',
                                         $tr);
    return [$cpp->getEssentialProperties()];
  }

  /**
   * @return \PHPUnit\Framework\MockObject\MockObject
   */
  private function getTokenMock() {
    $token = $this->createMock('Drupal\Core\Utility\Token');
    $token->expects($this->any())
      ->method('replace')
      ->will($this->returnArgument(0));

    return $token;
  }

  /**
   * Get TimezoneProviderInterface mock.
   */
  private function getTzpMock() {
    $tzp = $this->createMock('Drupal\ics_field\Timezone\TimezoneProviderInterface');
    $tzp->expects($this->any())
      ->method('getTimezoneString')
      ->will($this->returnValue('Europe/Zurich'));

    return $tzp;
  }

  /**
   * @return \PHPUnit\Framework\MockObject\MockObject
   */
  private function getTranslationManagerMock() {

    $tr = $this->createMock('Drupal\Core\StringTranslation\TranslationManager');
    $tr->expects($this->any())
      ->method('translate')
      ->will($this->returnArgument(0));

    return $tr;
  }

  /**
   * @return \PHPUnit\Framework\MockObject\MockObject
   */
  private function getContentEntityMock() {

    $ce = $this->createMock('Drupal\Core\Entity\ContentEntityInterface');
    $ce->expects($this->any())
      ->method('uuid')
      ->will($this->returnValue('i_am_the_uuid'));
    $ce->expects($this->any())
      ->method('getEntityTypeId')
      ->will($this->returnValue('node'));

    // Method get needs to return some mocks.
    $il = $this->createMock('Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList');
    // Returning a value that is not a instanceof DrupalDateTime will result in the datetime being skipped
    // this is enough to get our tests working here.
    $il->expects($this->any())
      ->method('getValue')
      ->will($this->returnValue(['value' => NULL]));

    $ce->expects($this->any())
      ->method('get')
      ->will($this->returnValue($il));

    return $ce;
  }

  /**
   * @return \PHPUnit\Framework\MockObject\MockObject
   */
  private function getContentEntityWithDateTimeMock($datetimeString = '2020-11-17T00:00:00') {

    $ce = $this->createMock('Drupal\Core\Entity\ContentEntityInterface');
    $ce->expects($this->any())
      ->method('uuid')
      ->will($this->returnValue('i_am_the_uuid'));
    $ce->expects($this->any())
      ->method('getEntityTypeId')
      ->will($this->returnValue('node'));

    // Method get needs to return some mocks.
    $il = $this->createMock('Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList');

    $dt = $this->createMock('Drupal\Core\Datetime\DrupalDateTime');
    $dt->expects($this->any())
      ->method('render')
      ->will($this->returnValue($datetimeString));

    $il->expects($this->any())
      ->method('getValue')
      ->will($this->returnValue([['value' => $dt]]));

    $ce->expects($this->any())
      ->method('get')
      ->will($this->returnValue($il));

    return $ce;
  }

}
