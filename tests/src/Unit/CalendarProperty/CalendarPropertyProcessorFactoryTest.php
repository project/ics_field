<?php

namespace Drupal\Tests\ics_field\Unit\CalendarProperty;

use Drupal\ics_field\CalendarProperty\CalendarPropertyProcessorFactory;
use Drupal\Tests\UnitTestCase;

/**
 * A class for testing the CalendarPropertyProcessorFactory.
 *
 * @group ics_field
 */
class CalendarPropertyProcessorFactoryTest extends UnitTestCase {

  /**
   * Test making the class with the correct dependencies.
   */
  public function testInstantiation() {

    $tpi = $this->createMock('Drupal\ics_field\Timezone\TimezoneProviderInterface');
    $t = $this->createMock('Drupal\Core\Utility\Token');
    $f = new CalendarPropertyProcessorFactory($tpi, $t);

    $this->assertInstanceOf('Drupal\ics_field\CalendarProperty\CalendarPropertyProcessorFactory',
                            $f);

  }

  /**
   * Test generating output.
   */
  public function testGeneration() {

    $tpi = $this->createMock('Drupal\ics_field\Timezone\TimezoneProviderInterface');
    $t = $this->createMock('Drupal\Core\Utility\Token');
    $f = new CalendarPropertyProcessorFactory($tpi, $t);

    $fdi = $this->createMock('Drupal\Core\Field\FieldDefinitionInterface');
    $fdi->expects($this->once())
      ->method('getSetting')
      ->will($this->returnValue('I am the reference'));

    $fci = $this->createMock('Drupal\Core\Field\FieldConfigInterface');
    $fci->expects($this->once())
      ->method('uuid')
      ->will($this->returnValue('i am the uuid'));

    $fdi->expects($this->once())
      ->method('getConfig')
      ->will($this->returnValue($fci));

    $this->assertInstanceOf('Drupal\ics_field\CalendarProperty\CalendarPropertyProcessor',
                            $f->create($fdi));
  }

}
