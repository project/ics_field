<?php

namespace Drupal\Tests\ics_field\Functional;

use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Tests that the add/edit Node Forms behaves properly.
 *
 * @group ics_field
 */
class CalendarDownloadNodeFormTest extends BrowserTestBase {

  /**
   * The admin user used in the tests.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Exempt from strict schema checking.
   *
   * @var bool
   *
   * @see \Drupal\Core\Config\Testing\ConfigSchemaChecker
   */
  protected $strictConfigSchema = FALSE;

  /**
   * A node created for testing.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $testNode;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';
  
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'field_ui',
    'node',
    'datetime',
    'ics_field',
    'file',
  ];

  const BUNDLE = 'ics_test';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([], NULL, 1);
    $this->adminUser->set('timezone', 'Europe/Zurich');
    $this->adminUser->save();

    $entityTypeManager = $this->container->get('entity_type.manager');
    $entityDisplayRepository = $this->container->get('entity_display.repository');
    $fieldStorageConfig = $entityTypeManager->getStorage('field_storage_config');
    $fieldConfig = $entityTypeManager->getStorage('field_config');
    $formDisplay = $entityDisplayRepository->getFormDisplay('node', static::BUNDLE, 'default');
    $viewDisplay = $entityDisplayRepository->getViewDisplay('node', static::BUNDLE, 'default');

    $this->drupalLogin($this->adminUser);

    $nodeType = NodeType::create([
      'type'        => static::BUNDLE,
      'name'        => 'ics_test',
      'description' => "Use <em>ics_test</em> for  testing ics.",
    ]);
    $nodeType->save();

    $fieldStorageConfig->create(
                  [
                    'field_name'    => 'field_dates',
                    'entity_type'   => 'node',
                    'type'          => 'datetime',
                    'datetime_type' => 'datetime',
                  ])->save();
    $fieldConfig->create(
                  [
                    'field_name'  => 'field_dates',
                    'label'       => 'Dates',
                    'entity_type' => 'node',
                    'bundle'      => static::BUNDLE,
                  ])->save();
    // Need to set the widget type, otherwise the form will not contain it.
    $formDisplay->setComponent('field_dates', ['type' => 'datetime_default'])
      ->save();

    $fieldIcsDownload = $fieldStorageConfig->create(
                                      [
                                        'field_name'  => 'field_ics_download',
                                        'entity_type' => 'node',
                                        'type'        => 'calendar_download_type',
                                      ]);
    $fieldIcsDownload->setSettings([
      'date_field_reference' => 'field_dates',
      'is_ascii'             => FALSE,
      'uri_scheme'           => 'public',
      'file_directory'       => 'icsfiles',
    ]);
    $fieldIcsDownload->save();
    $fieldConfig->create(
                  [
                    'field_name'  => 'field_ics_download',
                    'label'       => 'ICS Download',
                    'entity_type' => 'node',
                    'bundle'      => static::BUNDLE,
                  ])->save();
    // Need to set the widget type, otherwise the form will not contain it.
    $formDisplay->setComponent('field_ics_download', ['type' => 'calendar_download_default_widget'])
      ->save();
    $viewDisplay->setComponent('field_ics_download', [
                       'type'     => 'calendar_download_default_formatter',
                       'label' => 'ICS Download',
                       'settings' => [],
                     ])
      ->save();

    $fieldStorageConfig->create(
                  [
                    'field_name'  => 'field_body',
                    'entity_type' => 'node',
                    'type'        => 'text_with_summary',
                  ])->save();
    $fieldConfig->create(
                  [
                    'field_name'  => 'field_body',
                    'label'       => 'Body',
                    'entity_type' => 'node',
                    'bundle'      => static::BUNDLE,
                  ])->save();
    // Need to set the widget type, otherwise the form will not contain it.
    $formDisplay->setComponent('field_body',
                     [
                       'type'     => 'text_textarea_with_summary',
                       'settings' => [
                         'rows'         => '9',
                         'summary_rows' => '3',
                       ],
                       'weight'   => 5,
                     ])
      ->save();
  }

  /**
   * Test that we can add a node.
   */
  public function testCreateAndViewNode() {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet('user/' . $this->adminUser->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    // Create a random date in the coming week.
    $timestamp = $this->container->get('datetime.time')->getRequestTime() + random_int(0, 86400 * 7);
    $dateValue0Date = gmdate(DateTimeItemInterface::DATE_STORAGE_FORMAT, $timestamp);
    $dateValue0Time = gmdate('H:i:s', $timestamp);

    $nonexistentToken = '[node:nonexistenttoken_3047353]';
    $add = [
      'title[0][value]'                    => 'A calendar event',
      'field_dates[0][value][date]'        => $dateValue0Date,
      'field_dates[0][value][time]'        => $dateValue0Time,
      'field_body[0][value]'               => 'Lorem ipsum.',
      'field_ics_download[0][summary]'     => '[node:title]' . $nonexistentToken,
      'field_ics_download[0][description]' => '[node:field_body]',
      'field_ics_download[0][url]'         => '[node:url:absolute]',
    ];
    $this->drupalGet('node/add/ics_test');
    $this->submitForm($add, t('Save'));

    // Check that the node exists in the database.
    $node = $this->drupalGetNodeByTitle($add['title[0][value]']);
    $this->assertIsObject($node, 'Node found in database.');

    // Get the node's view.
    $this->drupalGet('node/' . $node->id());

    // Check if there is a link for downloading the ics file.
    $elements = $this->xpath('//a[@href and string-length(@href)!=0 and text() = :label]',
                             [':label' => t('iCal Download')->render()]);
    $el = reset($elements);
    $url = Url::fromUri('base:' . $el->getAttribute('href'), ['absolute' => TRUE]);    
    $icsString = file_get_contents($url->toString());
    $icsStringLinesArray = file($url->toString());
    $icsStringLinesArray = array_map('trim', $icsStringLinesArray);

    // Making sure unreplaced tokens get removed.
    // @see #3047353
    $this->assertEmpty(preg_grep(
      '/^SUMMARY:' . $add['title[0][value]'] . $nonexistentToken . '$/',
      $icsStringLinesArray));
    $this->assertNotEmpty(preg_grep(
      '/^SUMMARY:' . trim($add['title[0][value]']) . '$/',
      $icsStringLinesArray));

    $icalValidationUrl = $this->getExternalCalendalValidationService();

    if ($icalValidationUrl) {
      // Send a post to the ical_validation endpoint.
      $httpClient = new Client();
      $postArray = [
        'body' => $icsString,
      ];
      $response = $httpClient->post($icalValidationUrl, $postArray);
      $response = $response->getBody()->getContents();
      $this->assertNotEquals(FALSE, strpos($response, 'Validation successful'), $response);
    }
    else {
      // TODO Implement some local validation.
      // This would imply a need some local code iCal parsing library to
      // validate the generated string.
    }
  }

  /**
   * Check if ical_validation_url is available.
   *
   * @return string|bool
   *   Returns the ical validation service url, if the website is available,
   *   false otherwise.
   */
  private function getExternalCalendalValidationService() {
    $icalValidationUrl = 'http://icalvalidator:8080/validate';

    $curlInit = curl_init();
    curl_setopt($curlInit, CURLOPT_URL, $icalValidationUrl);
    curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curlInit, CURLOPT_HEADER, FALSE);
    curl_setopt($curlInit, CURLOPT_NOBODY, FALSE);
    curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, TRUE);
    try {
      $response = curl_exec($curlInit);
    }
    catch (ClientException $clientException) {
      $response = NULL;
    }
    finally {
      curl_close($curlInit);
    }
    if ($response) {
      return $icalValidationUrl;
    }
    return FALSE;
  }

}
